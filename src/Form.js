import React, { Component } from "react";
import Form, { Field } from "@atlaskit/form";
import Button from "@atlaskit/button";
import TextField from "@atlaskit/textfield";

import styled from "styled-components";

const Container = styled.div`
  display: flex;
  width: 400px;
  maxwidth: 100%;
  margin: 0 auto;
  flexdirection: column;
  text-align: center;
`;

class FormAtlassian extends Component {
  render() {
    return (
      <Container>
        <Form onSubmit={data => console.log("form data", data)}>
          {({ formProps }) => (
            <form {...formProps}>
              <Field
                name="username"
                defaultValue=""
                label="User name"
                isRequired
              >
                {({ fieldProps }) => <TextField {...fieldProps} />}
              </Field>
              <Button type="submit" appearance="primary">
                Submit
              </Button>
            </form>
          )}
        </Form>
      </Container>
    );
  }
}

export default FormAtlassian;
