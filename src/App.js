import React from "react";

import Calendar from "@atlaskit/calendar";
import "./App.css";

import Form from "./Form";
function App() {
  const log = msg => e => console.log(msg, e);
  return (
    <div className="App">
      <Calendar
        defaultDisabled={["2020-12-04"]}
        defaultPreviouslySelected={["2020-12-06"]}
        defaultSelected={["2020-12-08"]}
        defaultMonth={12}
        defaultYear={2020}
        innerProps={{
          style: {
            border: "1px solid red",
            display: "inline-block"
          }
        }}
        onBlur={log("blur")}
        onChange={log("change")}
        onFocus={log("focus")}
        onSelect={log("select")}
      />
      <Form />
    </div>
  );
}

export default App;
